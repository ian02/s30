/*
Insert in MongoDB (add record - POST request)
Retrieve records from MongoDB (GET Request)

/tasks - GET Request (Retrieve all tasks)
/tasks - POST Request (Add a task)

PROCESS:

While the folder is open in the terminal:
------
1) Create a folder called s30
2) Make a file called index.js
3) Open your terminal to s30
4) Login to MongoDB Atlas
5) inside s30 npm init
6) npm install express
7) import express
8) create express app
9) listen to port
10) import middlewares
11) listen method for requests
12) install mongoose (npm install mongoose)
13) import mongoose (const mongoose = require ('mongoose');)
14) Go to MongoDB Network Access - Allow Access from anywhere (0.0.0.0)
15) Get connection string

	Connect your application string
	
	mongodb+srv://admin:admin1234@zuitt-bootcamp.ktm2j.mongodb.net/myFirstDatabase?retryWrites=true&w=majority

16) Change myFirstDatabase to s30.  MongoDB will automatically create the database for us

	mongodb+srv://admin:admin1234@zuitt-bootcamp.ktm2j.mongodb.net/s30?retryWrites=true&w=majority

17) Connecting to MongoDB Atlas - add .connect() method
	mongoose.connect ();

18a) Add the connection string as the first argument

	mongoose.connect ('mongodb+srv://admin:admin1234@zuitt-bootcamp.ktm2j.mongodb.net/s30?retryWrites=true&w=majority')

18b) Add this object to allow connection

	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}


		mongoose.connect ('mongodb+srv://admin:admin1234@zuitt-bootcamp.ktm2j.mongodb.net/s30?retryWrites=true&w=majority', 
			{
				useNewUrlParser: true,
				useUnifiedTopology: true
			}
		);

19) Set notification for connection success or failure by using .connection property of mongoose
20) Store no. 19 in a variable called db
21) console.error.bind(console) allows us to print errors in the browser console and in the terminal
22) If the connection is successful, output this in the console.
23) Schemas determine the structure of the documents to be written in the database.  
	Schemas act as blueprints to our data.

	const taskSchema = new mongoose.Schema({
		//define the fields with the corresponding data type
		//for a task, it needs a "task name" and "task status"
		//thes a field called "name" and its data type is "String"
	name: String,
		//there is a field called "status" that is a "String" and the default value is "pending"
	status: {
		type: String,
		default: "pending"
	}
});

24) Create a model
	Models use schemas and they act as the middleman from the server (JS code) to our database
	Must be in singular form and capitalized ("Task")
	The first parameter of the Mongoose model method indicates the collection in where to store the data
	collection will become "tasks" (plural form of the model "task")

25) Create route to add task
26)  Check if the task already exists.  Use the Task Model to interact with the task collection.
27) Getting all tasks collection
	Get all the contents of the taks collection via the Task model
-----

*/

//Import Express
const express = require ('express');

//Import Mongoose
const mongoose = require ('mongoose');

//Create express app 
const app = express ();

//Creat a const called port
const port = 3001;

//Setup server to handle data from requests 
//Allows our app to read JSON data (middleware)
app.use(express.json())
app.use(express.urlencoded({extended:true}));

//Listen method for requests
//app.listen (port, () => console.log (`Server running at port ${port}`));


//Steps 17-19 connection string
mongoose.connect ('mongodb+srv://admin:admin1234@zuitt-bootcamp.ktm2j.mongodb.net/s30?retryWrites=true&w=majority', 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);
	

//Connection property
let db = mongoose.connection;


//Step 21
db.on("error", console.error.bind(console, "connection error"));


//Step 22
db.once ("open", () => console.log ("We are connected to the cloud database"));


//23. Schemas
const taskSchema = new mongoose.Schema({
	//define the fields with the corresponding data type
	//for a task, it needs a "task name" and "task status"
	//thes a field called "name" and its data type is "String"
	name: String,
	//there is a field called "status" that is a "String" and the default value is "pending"
	status: {
		type: String,
		default: "pending"
	}
});


//24. Create a model
const Task = mongoose.model ("Task", taskSchema);

/*
Business Logic
1.  Add a functionality to check if there are duplicate tasks.
	-if the task already exists in the database, we return an error
	-if the task doesn't exist in the database, we add it in the database
*/


//25. Create route to add task
// app.post('/tasks', (req, res) => {
// 	req.body.name
// })



//26.  Check if the task already exists.  Use the Task Model to interact with the task collection.

/*
Eat
Look into the tasks collection
Task.findOne()
Task.findOne({name: 'Eat'})

HTML Form - POSTman
req.body
req.body.name

Submit registation
lname:
fname:
email:

req.body.email



result object (line 189)

name: 'Eat'
status: 'Pending'

Task.findOne({name: req.body.name}, () => {})
*/

app.post ('/tasks', (req, res) => {
	Task.findOne({name: req.body.name}, (err, result) => {
		if (result != null && result.name == req.body.name){
			//Return a message to the client/Postman
			res.send ('Duplicate task found!')
		} else {
			let newTask = new Task ({
				name: req.body.name
			})


/* This is the newTask looks like

	{
		name: 'Code',
		status: 'Pending'
	}
*/


		//newTask.save()
			newTask.save((saveErr, savedTask) => {
				if (saveErr){
					return console.error(saveErr)
				} else {
					return res.status (201).send('New task created.')
				}

			})
		}

	})
})


//27. Get all tasks
app.get('/tasks', (req, res) => {
	Task.find({}, (err, result) => {
		if (err){
			return console.log(err)
		} else {
			return res.status (200).json({data: result})
		}
	}) 
})

//Activity

//1. Create a User schema
const userSchema = new mongoose.Schema({
	username: String,
	password: String
});

//2. Make a Model
const User = mongoose.model ("User", userSchema);


//3. Register a User


app.post ('/signup', (req, res) => {
	User.findOne({username: req.body.username}, (err, result) => {
		if (result != null && result.username == req.body.username){
			res.send ('Duplicate username found!')
		} else {

			//if (req.body.username !== '' && req.body.password !=='')

			let newUser = new User ({
				username: req.body.username,
				password: req.body.password
			})

			newUser.save((saveErr, savedTask) => {
				if (saveErr){
					return console.error(saveErr)
				} else {
					return res.status (201).send('New user created.')
				}

			})
		}

	})
})


app.get('/signup', (req, res) => {
	User.find({}, (err, result) => {
		if (err){
			return console.log(err)
		} else {
			return res.status (200).json({data: result})
		}
	}) 
})


app.listen (port, () => console.log (`Server running at port ${port}`));